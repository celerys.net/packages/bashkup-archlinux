#!/usr/bin/bash

# shellcheck disable=SC2034
# shellcheck disable=SC2154

# Maintainer: Hugo Trassoudaine <hugo@trsd.pro>

_ver="1.0.8"
_rel="1"

pkgbase='bashkup'
pkgname=(
    'bashkup-node'
    'bashkup-master'
)
pkgver="${_ver}"
pkgrel="${_rel}"
pkgdesc='Pure Bash backup solution around rsnapshot'
url='https://gitlab.com/celerys.net/packages/bashkup'
arch=( 'any' )
license=( 'GPL3' )
source=(
    "bashkup-${_ver}.tar::https://gitlab.com/api/v4/projects/52709165/repository/archive.tar?sha=v${_ver}"
)
sha256sums=(
    '7f33098ebc02468abd0c34ce67bcd95cd8d287637210ccc3289cf06fe83ab26f'
)
validpgpkeys=( )
makedepends=(
    'findutils'  # find
)
depends=(
    'bash>=5'
    'coreutils'
    'findutils'  # find
    'gettext'    # envsubst
    'grep'       # grep
    'glibc'      # getent
)
backup=( )

package_bashkup-node() {
    # shellcheck disable=SC2164
    cd "${srcdir}/bashkup-v${_ver}-"*

    pkgdesc="${pkgdesc}: generic node package"

    depends+=(
        'sudo'
        'rsync'
    )

    install -dm00755 "${pkgdir}/usr/lib/sysusers.d"
    install -m00644 sysusers/* "${pkgdir}/usr/lib/sysusers.d/"

    install -dm00755 "${pkgdir}/usr/lib/tmpfiles.d"
    install -m00644 tmpfiles/* "${pkgdir}/usr/lib/tmpfiles.d/"

    install -dm00750 "${pkgdir}/etc/sudoers.d"
    install -m00440 sudoers/* "${pkgdir}/etc/sudoers.d/"
}

package_bashkup-master() {
    # shellcheck disable=SC2164
    cd "${srcdir}/bashkup-v${_ver}-"*

    pkgdesc="${pkgdesc}: master node package"

    depends+=(
        'bashkup-node'
    )

    backup+=(
        'etc/bashkup/bashkup.conf'
        'etc/bashkup/ssh.conf'
    )

    install -dm00755 "${pkgdir}/etc/bashkup"
    install -m00600 config/bashkup.conf "${pkgdir}/etc/bashkup/"
    install -m00644 config/ssh.conf "${pkgdir}/etc/bashkup/"

    install -dm00755 "${pkgdir}/usr/bin"
    install -m00755 bashkup "${pkgdir}/usr/bin/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup"
    install -m00755 bin/* "${pkgdir}/usr/lib/bashkup/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/core"
    install -m00755 modules/core/* "${pkgdir}/usr/lib/bashkup/modules/core/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup/systemd"
    while IFS= read -r -d $'\0' file_rel_src; do
        # Install with subdirectories.
        install -Dm00644 "systemd/services/${file_rel_src}" \
            "${pkgdir}/usr/lib/bashkup/systemd/${file_rel_src}"
    done < <(cd systemd/services && find . -type f -print0)

    install -dm00755 "${pkgdir}/usr/lib/systemd/system"
    while IFS= read -r -d $'\0' file_rel_src; do
        # Install with subdirectories.
        install -Dm00644 "systemd/timers/${file_rel_src}" \
            "${pkgdir}/usr/lib/systemd/system/${file_rel_src}"
    done < <(cd systemd/timers && find . -type f -print0)

    install -dm00755 "${pkgdir}/usr/lib/systemd/system-generators/"
    ln -s "/usr/lib/bashkup/bashkup-systemd-generator" \
        "${pkgdir}/usr/lib/systemd/system-generators/bashkup-systemd-generator"

    _include_module_bundle
    _include_module_collect
    _include_module_snapshot
    _include_module_remote
}

_include_module_bundle() {
    depends+=(
        'tar'
        'xz'
        'openssl>=3'
        'rclone'
    )

    backup+=(
        'etc/bashkup/rclone.conf'
    )

    install -dm00755 "${pkgdir}/etc/bashkup"
    install -m00600 config/rclone.conf "${pkgdir}/etc/bashkup/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/plugins"
    install -m00755 modules/plugins/bundle.sh \
        "${pkgdir}/usr/lib/bashkup/modules/plugins/"
}

_include_module_collect() {
    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/plugins"
    install -m00755 modules/plugins/collect.sh \
        "${pkgdir}/usr/lib/bashkup/modules/plugins/"
}

_include_module_snapshot() {
    depends+=(
        'rsnapshot'
    )

    backup+=(
        'etc/bashkup/rsnapshot.conf'
        'etc/bashkup/rsync.conf'
    )

    install -dm00755 "${pkgdir}/etc/bashkup"
    install -dm00755 "${pkgdir}/etc/bashkup/rsnapshot.d"
    install -m00644 config/rsnapshot.conf "${pkgdir}/etc/bashkup/"
    install -m00644 config/rsync.conf "${pkgdir}/etc/bashkup/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/plugins"
    install -m00755 modules/plugins/snapshot.sh \
        "${pkgdir}/usr/lib/bashkup/modules/plugins/"
}

_include_module_remote() {
    depends+=(
        'openssh'
        'rsync'
    )

    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/plugins"
    install -m00755 modules/plugins/remote.sh \
        "${pkgdir}/usr/lib/bashkup/modules/plugins/"

    install -dm00755 "${pkgdir}/usr/lib/bashkup/modules/plugins/remote"
    install -m00755 modules/plugins/remote/* \
        "${pkgdir}/usr/lib/bashkup/modules/plugins/remote/"
}
